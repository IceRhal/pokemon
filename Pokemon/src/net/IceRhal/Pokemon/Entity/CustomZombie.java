package net.IceRhal.Pokemon.Entity;

import java.lang.reflect.Field;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R1.util.UnsafeList;

import net.minecraft.server.v1_8_R1.Blocks;
import net.minecraft.server.v1_8_R1.EntityZombie;
import net.minecraft.server.v1_8_R1.GenericAttributes;
import net.minecraft.server.v1_8_R1.ItemStack;
import net.minecraft.server.v1_8_R1.PathfinderGoalFloat;
import net.minecraft.server.v1_8_R1.PathfinderGoalMeleeAttack;
import net.minecraft.server.v1_8_R1.PathfinderGoalSelector;
import net.minecraft.server.v1_8_R1.World;

public class CustomZombie extends EntityZombie {

	int team;
	
	public CustomZombie(World w) {	
		
		super(w);
		
		this.datawatcher.watch(2, "Zombie");
		
		team = new Random().nextInt(16) - 1;
		
		setEquipment(4, new ItemStack(Blocks.WOOL, 1, team));
		
		try {
		
			Field bField = PathfinderGoalSelector.class.getDeclaredField("b");
			bField.setAccessible(true);
			Field cField = PathfinderGoalSelector.class.getDeclaredField("c");
			cField.setAccessible(true);
			bField.set(goalSelector, new UnsafeList<PathfinderGoalSelector>());
			bField.set(targetSelector, new UnsafeList<PathfinderGoalSelector>());
			cField.set(goalSelector, new UnsafeList<PathfinderGoalSelector>());
			cField.set(targetSelector, new UnsafeList<PathfinderGoalSelector>());
				
		} catch (Exception exc) {
				
			exc.printStackTrace();
		}
			 
		this.goalSelector.a(0, new PathfinderGoalFloat(this));
		//this.goalSelector.a(2, new PathfinderGoalWalktoTile(this, 1.0F));
		this.goalSelector.a(2, new PathfinderGoalMeleeAttack(this, CustomZombie.class, 1.0D, false));
		
		this.targetSelector.a(2, new CustomPathfinderGoalNearestAttackableTarget(this, CustomZombie.class, true));
	}
	
	public void spawn(Location l) {
		
		this.setLocation(l.getX(), l.getY(), l.getZ(), l.getYaw(), l.getPitch());
		((CraftWorld)l.getWorld()).getHandle().addEntity(this);
	}
	
    @Override
    protected void aW() {
    	
    	super.aW();
    	this.getAttributeInstance(GenericAttributes.b).setValue(60.0D); // Original 3.0D
    }
}
