package net.IceRhal.Pokemon.Entity;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.google.common.base.Predicates;

import net.minecraft.server.v1_8_R1.EntityCreature;
import net.minecraft.server.v1_8_R1.EntityLiving;
import net.minecraft.server.v1_8_R1.IEntitySelector;
import net.minecraft.server.v1_8_R1.PathfinderGoalNearestAttackableTarget;

public class CustomPathfinderGoalNearestAttackableTarget extends PathfinderGoalNearestAttackableTarget {

	private final int g = 0;
	  
	public CustomPathfinderGoalNearestAttackableTarget(
			EntityCreature entitycreature, @SuppressWarnings("rawtypes") Class oclass, boolean flag) {
		
		super(entitycreature, oclass, flag);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean a() {
		
	    if ((this.g > 0) && (this.e.bb().nextInt(this.g) != 0)) {
	        return false;
	      }
	    
	    double d0 = f();
	    List list = this.e.world.a(this.a, this.e.getBoundingBox().grow(d0, 4.0D, d0), Predicates.and(this.c, IEntitySelector.d));

	    Collections.sort(list, this.b);
	    if (list.isEmpty()) {
	    	return false;
	    }	    
	    this.d = ((EntityLiving)list.get(0));
	    
	    if(!(this.e instanceof CustomZombie)) return true;
	    
	    Iterator it = list.iterator();
	    
	    CustomZombie z1 = (CustomZombie) this.e;
	    
	    while(it.hasNext()) {
	    	
	    	this.d = (EntityLiving) it.next();
	    	
	    	if(!(this.d instanceof CustomZombie)) return true;
	    	
	    	CustomZombie z2 = (CustomZombie) this.d;
	    	
	    	if(z1.team != z2.team) return true;
	    }
	    
	    return false;
		
		
	}

}
