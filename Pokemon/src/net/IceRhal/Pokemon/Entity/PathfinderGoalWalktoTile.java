package net.IceRhal.Pokemon.Entity;

import net.minecraft.server.v1_8_R1.EntityCreature;
import net.minecraft.server.v1_8_R1.PathfinderGoal;

public class PathfinderGoalWalktoTile extends PathfinderGoal {

	float speed;
	private EntityCreature entitycreature;
	
	public PathfinderGoalWalktoTile(EntityCreature entitycreature, float speed) {
		
		this.speed = speed;
		this.entitycreature = entitycreature;	
	}
	 	 
	public boolean a() {
				
		return true;
	}
	
	 public boolean b() {
		 		 
		 return !this.entitycreature.getNavigation().m();
	}
	 	
	public void c() {
		
		this.entitycreature.getNavigation().a(-1, 63, -5, speed);		
	}

}
