package net.IceRhal.Pokemon.Entity;

import java.lang.reflect.Field;
import java.util.Map;

import net.minecraft.server.v1_8_R1.Entity;
import net.minecraft.server.v1_8_R1.EntityTypes;

@SuppressWarnings({"rawtypes", "unchecked"})
public enum Entities {
	
	CUSTOM_ZOMBIE("Zombie", 54, CustomZombie.class);
	
    private Entities(String name, int id, Class<? extends Entity> custom) {
    	
        addToMaps(custom, name, id);
    }
    
    private Entities(String name, int id, Class<? extends Entity> custom, boolean overrideDefault) {
    	    	
        if(overrideDefault) override(custom, name, id);
        else addToMaps(custom, name, id);        
    }
	
	private static void addToMaps(Class clazz, String name, int id) {
		
        ((Map)getPrivateField("d", EntityTypes.class, null)).put(clazz, name);
        ((Map)getPrivateField("f", EntityTypes.class, null)).put(clazz, Integer.valueOf(id));
    }
	
	private static void override(Class clazz, String name, int id) {
		
		addToMaps(clazz, name, id);
		
		((Map)getPrivateField("c", EntityTypes.class, null)).put(name, clazz);
        ((Map)getPrivateField("e", EntityTypes.class, null)).put(Integer.valueOf(id), clazz);
        ((Map)getPrivateField("g", EntityTypes.class, null)).put(name, Integer.valueOf(id));
	}	
	
	public static Object getPrivateField(String fieldName, Class clazz, Object object) {
		
        Field field;
        Object o = null;

        try {
            field = clazz.getDeclaredField(fieldName);

            field.setAccessible(true);

            o = field.get(object);
        } catch(NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
        	
            e.printStackTrace();
        }

        return o;
    }
	
	public static void load() {}
}
