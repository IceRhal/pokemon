package net.IceRhal.Pokemon;

import net.IceRhal.Pokemon.Entity.CustomZombie;
import net.IceRhal.Pokemon.Entity.Entities;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R1.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Pokemon extends JavaPlugin {
	
	public void onEnable() {
		
		Entities.load();
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {		
				
		if(!(sender instanceof Player)) return false;
		
		Player p = (Player) sender;
		
		
		if(label.equalsIgnoreCase("meuh")) {			
						
			p.sendMessage(ChatColor.GREEN + "Custom Zombie spawn");		
			if(args.length == 1) {
				
				for(int x = Integer.parseInt(args[0]); x > 0; x--) {
					
					new CustomZombie(((CraftWorld)p.getWorld()).getHandle()).spawn(p.getLocation());
				}
			}
			else new CustomZombie(((CraftWorld)p.getWorld()).getHandle()).spawn(p.getLocation());			
			
			return true;
		}			
		
		return false;		
	}
}
